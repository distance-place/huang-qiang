import sys
import os
import keyboard
import time

startPage = 10
onePage = 2


def nextPage():
    pageFile = open("page", "r+")
    os.system("cls")
    global page
    global TextArr, onLine, onPageTxt
    page += 1
    onPageTxt = TextArr[page].split("\n")
    onLine = 0
    mprint(startPage)
    pageFile.write(str(page))
    pageFile.close()
    return


def previous():
    pageFile = open("page", "r+")
    os.system("cls")
    global page
    global TextArr, onLine, onPageTxt
    page -= 1
    onPageTxt = TextArr[page].split("\n")
    onLine = 0
    mprint(startPage)
    pageFile.write(str(page))
    pageFile.close()
    return


Bottom = 2


def mprint(line):
    global onLine, Bottom, onPageTxt
    onLine += line
    if onLine > len(onPageTxt):
        print("到尾部了 空格下一页")
        line -= onLine - len(onPageTxt)
        onLine = len(onPageTxt)
        if Bottom == 1:
            nextPage()
            return
        Bottom = 1
        return
    elif onLine < 0:
        previous()
        return
    while line > 0:
        print(onPageTxt[onLine - line])
        line -= 1
    Bottom = 0
    return


def down():
    mprint(onePage)
    return


def up():
    global onLine
    os.system("cls")
    onLine = 0
    mprint(startPage)
    return


prvar = 0


def protective():
    global prvar
    if prvar is 0:
        global autovar
        autovar = 0
        os.system("cls")
        print("hellow java")
        prvar = 1
    else:
        global onLine, onPageTxt
        os.system("cls")
        prvar = 0
        for i in range(onLine):
            print(onPageTxt[i])


autovar = 0
vspeed = 2

try:
    vspeedfile = open("vspeed", "r+")
    vspeed = float(vspeedfile.read())
except:
    vspeedfile = open("vspeed", "w+")
    vspeedfile.write(str(vspeed))
    pass

vspeedfile.close()


def slowDown():
    global vspeed
    vspeedfile = open("vspeed", "w")
    vspeed -= 0.1
    if vspeed < 0.1:
        vspeed = 0.1000
    vspeedfile.write(str(vspeed))
    vspeedfile.close()
    print("当前速度:" + str(vspeed))
    pass


def speedup():
    global vspeed
    vspeedfile = open("vspeed", "w")
    vspeed += 0.1
    vspeedfile.write(str(vspeed))
    vspeedfile.close()
    print("当前速度:" + str(vspeed))
    pass


def autoPage():
    global autovar
    if autovar is 0:
        autovar = 1
    else:
        autovar = 0


varShortcuts = True


def switchShortcuts():
    global varShortcuts
    if varShortcuts:
        print("开启快捷键")
        keyboard.add_hotkey("space", nextPage)
        keyboard.add_hotkey("shift+space", previous)
        keyboard.add_hotkey("up", up)
        keyboard.add_hotkey("down", down)
        keyboard.add_hotkey("0", autoPage)
        keyboard.add_hotkey(".", protective)
        keyboard.add_hotkey("left", slowDown)
        keyboard.add_hotkey("right", speedup)
        varShortcuts = False
    else:
        print("关闭快捷键")
        keyboard.remove_hotkey("space")
        keyboard.remove_hotkey("shift+space")
        keyboard.remove_hotkey("up")
        keyboard.remove_hotkey("down")
        keyboard.remove_hotkey("0")
        keyboard.remove_hotkey(".")
        keyboard.remove_hotkey("left")
        keyboard.remove_hotkey("right")
        varShortcuts = True


TextFile = open("全职法师.txt", "rb+")
Text = TextFile.read()
Text = Text.decode(encoding="utf-8")
try:
    pageFile = open("page", "r+")
    page = pageFile.read()
except:
    pageFile = open("page", "w")
if page is None or page is "":
    page = 0
    pageFile.write(str(page))
else:
    page = int(page)
if Text is None or Text is "":
    print("txt没有数据 运行结束")
    sys.exit()
TextArr = Text.split("========")

TextFile.close()
pageFile.close()
onPageTxt = TextArr[page].split("\n")
onLine = 0
mprint(startPage)

# 注册快捷键
switchShortcuts()
keyboard.add_hotkey("ctrl+.", switchShortcuts)

# keyboard.wait()
while 1:
    time.sleep(vspeed)
    if autovar is 1:
        down()
    pass

# while 1:
#     pass
